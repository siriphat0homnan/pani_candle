import React, { Component } from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import { SearchBar, Header } from 'react-native-elements';
import example from '../assets/images/example.png'
import InputSpinner from "react-native-input-spinner"

export default class CartScreen extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View style={styles.container}>
                <Header
                    leftComponent={{
                        icon: 'keyboard-backspace',
                        color: '#000',
                        onPress: () => this.props.navigation.navigate('Home'),
                    }}
                    centerComponent={{ text: 'Pani Candle', style: { color: '#000', fontSize: 20 } }}
                    rightComponent={{
                        icon: 'check-circle',
                        color: '#000',
                        onPress: () => this.props.navigation.navigate('Home'),
                    }}
                    backgroundColor="#A7FFFC"
                />

                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                    <View style={{ flex: 1, height: 150, borderColor: '#FFFFF', borderWidth: 2 }}>
                        <View style={{ alignItems: 'center', alignContent: 'center' }}>
                            <Image source={example}
                                style={{
                                    margin: 20,
                                    width: 100, height: 100
                                }}
                            >
                            </Image>
                        </View>
                    </View>
                    <View style={{ flex: 1, height: 150, borderColor: '#FFFFF', borderWidth: 2 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{
                                margin: 20,
                            }}>products description</Text>
                            <InputSpinner
                                max={10}
                                min={2}
                                step={2}
                                colorMax={"#f04048"}
                                colorMin={"#40c5f4"}
                                //value={this.state.number}
                                onChange={(num) => {
                                    console.log(num);
                                }}
                            />

                        </View>
                    </View>
                    <View style={{ flex: 0.5, height: 150, borderColor: '#FFFFF', borderWidth: 2 }}>
                        <View style={{alignItems:'center',marginTop:20}}>
                            <Text>
                                DELETE
                        </Text>
                        </View>

                    </View>
                </View>

            </View>
        );
    }
}

CartScreen.navigationOptions = {
    title: 'LOGO',
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
    },
    search: {
        margin: 10,
        height: 1,
        backgroundColor: 'powderblue',
        flex: 1
    },
    catagory: {
        margin: 10,
        height: 1,
        backgroundColor: 'skyblue',
        flex: 2
    },
    products: {
        margin: 10,
        height: 1,
        backgroundColor: 'steelblue',
        flex: 3
    },
    headbar: {
        // height: 100,
        // backgroundColor: "",
        borderColor: '#FFFFFF',
        flexDirection: 'row',
        justifyContent: 'space-between'

    },
    itemHeadLeft: {
        backgroundColor: '#ABEBC6',
        padding: 20,
        alignItems: 'center',
    },
    itemHeadRight: {
        backgroundColor: '#D6EAF8',
        padding: 20,
        alignItems: 'center',
    }


});
