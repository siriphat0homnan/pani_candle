import React, { Component } from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Button, Dimensions} from 'react-native';
import { SearchBar, Header, ListItem, List, Image, CheckBox  } from 'react-native-elements';
import SectionedMultiSelect from "react-native-sectioned-multi-select";
// default size
const { width, height } = Dimensions.get('window');
// config height size
const height_config = 300;
const items2 = [
    // this is the parent or 'item'
    {
        name: 'Fruits',
        id: 0,
        // these are the children or 'sub items'
        children: [
            {
                name: 'Apple',
                id: 10,
            },
            {
                name: 'Strawberry',
                id: 17,
            },
            {
                name: 'Pineapple',
                id: 13,
            },
            {
                name: 'Banana',
                id: 14,
            },
            {
                name: 'Watermelon',
                id: 15,
            },
            {
                name: 'Kiwi fruit',
                id: 16,
            },
        ],

    },
    {
        name: 'เทียนหอม',
        id: 1,
        // these are the children or 'sub items'
        children: [
            {
                name: 'Apple',
                id: 20,
            },
            {
                name: 'Strawberry',
                id: 27,
            },
            {
                name: 'Pineapple',
                id: 23,
            },
            {
                name: 'Banana',
                id: 24,
            },
            {
                name: 'Watermelon',
                id: 25,
            },
            {
                name: 'Kiwi fruit',
                id: 26,
            },
        ],
    },

];
export default class LinksScreen extends Component{
    state = {
        search: '',
    };
    updateSearch = search => {
        this.setState({ search });
    };
    constructor() {
        super();
        this.state = {
            selectedItems: [],
        };
    }
    onSelectedItemsChange = (selectedItems) => {
        this.setState({ selectedItems });
    };

  render()
  {const { search } = this.state;
    return (
        <View style={styles.container}>
            <Header
                leftComponent={{
                    icon: 'g-translate',
                    color: '#000',
                    onPress: () => this.props.navigation.navigate('Cart'),
                }}
                centerComponent={{ text: 'Pani Candle', style: { color: '#000', fontSize: 20 } }}
                rightComponent={{
                    icon: 'shopping-basket',
                    color: '#000',
                    onPress: () => this.props.navigation.navigate('Cart'),
                }}
                backgroundColor="#A7FFFC"

            />
          <View style={{flex: 1, flexDirection: 'column'}}>
              <SearchBar
                  placeholder="Search..."
                  onChangeText={this.updateSearch}
                  value={search}
                  lightTheme
              />
              <SectionedMultiSelect
                  items={items2}
                  uniqueKey="id"
                  subKey="children"
                  selectText="Catagory"
                  showDropDowns={true}
                  readOnlyHeadings={true}
                  onSelectedItemsChange={this.onSelectedItemsChange}
                  selectedItems={this.state.selectedItems}
                  single={true}
              />
              <View style={styles.products}>
                  <Image
                      source={{ uri: 'http://www.panicandle.com/images/content/original-1552106357186.jpg' }}
                          style={{ width: width, height: height_config, alignItems: 'center'}}
                   />

                  <Text style={styles.getStartedText}>เทียนหอมในถ้วย,เทียนทีไลท์สี,เทียนทีไลท์กลิ่นหอม,เทียนถ้วยหอม,เทียนหอมถ้วย,เทียนหอมกลิ่นต่างๆ</Text>
                  <CheckBox
                      center
                      title='Add to Cart'
                      iconRight
                      iconType='material'
                      checkedIcon='clear'
                      uncheckedIcon='add'
                      checkedColor='red'
                      checked={this.state.checked}
                  />


              </View>
          </View>



        </View>

    );
  }
}

// function gotoCart() {
//   console.log("press");
//   props.navigation.navigate('Cart')
// }

LinksScreen.navigationOptions= {
    header: null,
  // headerLeft: (
  //     <Button
  //         onPress={() => alert('Change Language')}
  //         title="TH/EN"
  //         color="#1456EB"/>
  // ),
  // headerRight: (
  //     <Button
  //         onPress={() =>test}
  //         title="CART"
  //         color="#1456EB"
  //     />),
  // title: "Pani Candle",
  // headerTitleStyle: {
  //   textAlign:"center",
  //   flex:1
  // },
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
      flexDirection: 'column',
  },
  search: {
    margin: 10,
    height: 1,
    backgroundColor: 'powderblue',
    flex: 1
  },
  catagory: {
    margin: 10,
    height: 1,
    backgroundColor: 'skyblue',
    flex: 2
  },
  products: {

    height: 1,
    // backgroundColor: 'steelblue',
    flex: 3
  },
  headbar: {
    borderColor: '#fdfff8',
    flexDirection: 'row',
    justifyContent: 'space-between'

  },
  itemHeadLeft: {
    backgroundColor: '#ABEBC6',
    padding: 20,
    alignItems: 'center',
  },
  itemHeadRight: {
    backgroundColor: '#D6EAF8',
    padding: 20,
    alignItems: 'center',
  },
    getStartedText:{
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        lineHeight: 24,
        textAlign: 'center',
    }
});
