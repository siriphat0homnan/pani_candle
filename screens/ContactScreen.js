import React, { Component } from 'react';
import {StyleSheet, Text, View, Linking, Platform, Image, Button} from 'react-native';
import { Table, TableWrapper, Row, Rows, Col} from 'react-native-table-component';
import Touchable from 'react-native-platform-touchable';
import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from "expo-web-browser";
import { SearchBar, Header } from 'react-native-elements';


export default class ContactScreen extends Component {
    // link phone
    dialCall = () => {
        let phoneNumber = '';
        if (Platform.OS === 'android') {
            phoneNumber = 'tel:${0865989962}';
        }
        else {
            phoneNumber = 'telprompt:${0865989962}';
        }
        Linking.openURL(phoneNumber);
    };

    render() {
        return (
            <View style={styles.container }>
                <Header
                    leftComponent={{
                        icon: 'g-translate',
                        color: '#000',
                        onPress: () => this.props.navigation.navigate('Cart'),
                    }}
                    centerComponent={{ text: 'Pani Candle', style: { color: '#000', fontSize: 20 } }}
                    rightComponent={{
                        icon: 'shopping-basket',
                        color: '#000',
                        onPress: () => this.props.navigation.navigate('Cart'),
                    }}
                    backgroundColor="#A7FFFC"

                />
                <View style={{ flexDirection: 'row' }}>
                    <View style={styles.container}>
                        <Text style={styles.Topic}>Contact  <Ionicons name="ios-chatboxes" size={22} color="#ccc" /></Text>
                    </View>
                </View>
                <Touchable
                    style={styles.option}
                    background={Touchable.Ripple('#ccc', false)}
                   >
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.optionIconContainer}>
                            <Text style={styles.optionText}>Line: </Text>
                        </View>
                        <View>
                            <Text style={styles.optionText}>@panicandle</Text>
                        </View>
                    </View>
                </Touchable>
                <Touchable
                    style={styles.option}
                    background={Touchable.Ripple('#ccc', false)}
                    onPress={handleLinkWebsite}
                >
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.optionIconContainer}>
                            <Text style={styles.optionText}>Website: </Text>
                        </View>
                        <View>
                            <Text style={styles.optionText}>https://www.panicandle.com</Text>
                        </View>
                    </View>
                </Touchable>
                <Touchable
                    style={styles.option}
                    background={Touchable.Ripple('#ccc', false)}
                    onPress={handleLinkFacebook}
                >
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.optionIconContainer}>
                            <Text style={styles.optionText}>Facebook: </Text>
                        </View>
                        <View>
                            <Text style={styles.optionText}>panicandle</Text>
                        </View>
                    </View>
                </Touchable>
                <Touchable
                    style={styles.option}
                    background={Touchable.Ripple('#ccc', false)}
                    onPress={this.dialCall}
                >
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.optionIconContainer}>
                            <Text style={styles.optionText}>Telephone: </Text>
                        </View>
                        <View>
                            <Text style={styles.optionText}>086-5989962</Text>
                        </View>
                    </View>
                </Touchable>
                <Touchable
                    style={styles.option}
                    background={Touchable.Ripple('#ccc', false)}
                    onPress={handleLinkEmail}
                >
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.optionIconContainer}>
                            <Text style={styles.optionText}>Email: </Text>
                        </View>
                        <View>
                            <Text style={styles.optionText}>panicandle@gmail.com</Text>
                        </View>
                    </View>
                </Touchable>
            </View>
        );
    }
}
ContactScreen.navigationOptions = {
    header: null
    // headerLeft: (
    //     <Button
    //         onPress={() => alert('Change Language')}
    //         title="TH/EN"
    //         color="#1456EB"/>
    // ),
    // headerRight: (
    //     <Button
    //         onPress={() => alert('This is a button!')}
    //         title="CART"
    //         color="#1456EB"
    //     />),
    // title: "Pani Candle",
    // headerTitleStyle: {
    //     textAlign:"center",
    //     flex:1
    // },
};

// link facebook
function handleLinkFacebook() {
    Linking.openURL('https://www.facebook.com/panicandle');
}

// link website
function handleLinkWebsite() {
    WebBrowser.openBrowserAsync(
        'https://www.panicandle.com'
    );
}

// link email
function handleLinkEmail() {
    Linking.openURL('mailto:panicandle@gmail.com');
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    optionsTitleText: {
        fontSize: 16,
        marginLeft: 15,
        marginTop: 9,
        marginBottom: 12,
    },
    optionIconContainer: {
        marginRight: 9,
    },
    option: {
        backgroundColor: '#fdfdfd',
        paddingHorizontal: 15,
        paddingVertical: 15,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#EDEDED',
    },
    optionText: {
        fontSize: 15,
        marginTop: 1,
    },
    Topic:{
        fontSize: 30,
        textAlign: 'center',
    }
});
