import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  Button
} from 'react-native';
import Carousel from 'react-native-looped-carousel';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SearchBar, Header } from 'react-native-elements';
import LinksScreen from "./LinksScreen";

// --------------------- constant -------------------
// default size
const { width, height } = Dimensions.get('window');
// config height size
const height_config = 300;

export default class HomeScreen extends Component {
  // static navigationOptions = {
  //   headerRight: <button>Cart</button>,
  //   headerLeft: <button>TH/EN</button>,
  //   title: "Pani Candle",
  //   headerTitleStyle: {
  //     textAlign:"center",
  //     flex:1
  //   },
  // };

  constructor(props) {
    super(props);
    this.state = {
      size: { width, height },
    };
  }
  // set size layout Carousel
  _onLayoutDidChange = (e) => {
    const layout = e.nativeEvent.layout;
    this.setState({ size: { width: layout.width, height: height_config } });
  }

  render() {
    return (

      <View style={styles.container}>
        <Header
            leftComponent={{
              icon: 'g-translate',
              color: '#000',
              onPress: () => this.props.navigation.navigate('Cart'),
            }}
            centerComponent={{ text: 'Pani Candle', style: { color: '#000', fontSize: 20 } }}
            rightComponent={{
              icon: 'shopping-basket',
              color: '#000',
              onPress: () => this.props.navigation.navigate('Cart'),
            }}
            backgroundColor="#A7FFFC"
        />

        <View style={{ height: height_config }}>
          <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>
            <Carousel
              delay={6000}
              style={this.state.size}
              autoplay
              pageInfo
              onAnimateNextPage={(p) => console.log(p)}
            >
              <ImageBackground source={require('../assets/images/1.jpg')} style={this.state.size} />
              <ImageBackground source={require('../assets/images/2.jpg')} style={this.state.size} />
              <ImageBackground source={require('../assets/images/3.jpg')} style={this.state.size} />
              <ImageBackground source={require('../assets/images/4.jpg')} style={this.state.size} />
              <ImageBackground source={require('../assets/images/5.jpg')} style={this.state.size} />
            </Carousel>
          </View>
        </View>

        <ScrollView style={styles.container}>
          <View style={styles.getStartedContainer}>
            {/*<DevelopmentModeNotice />*/}
            <Text style={styles.getStartedText}>News / Messages Area</Text>
          </View>

        </ScrollView>
      </View>
    );
  }
}


HomeScreen.navigationOptions = {
  header: null,
};



// function DevelopmentModeNotice() {
//   if (__DEV__) {
//     const learnMoreButton = (
//       <Text onPress={handleLearnMorePress} style={styles.helpLinkText}>
//         Learn more
//       </Text>
//     );

//     return (
//       <Text style={styles.developmentModeText}>
//         Development mode is enabled: your app will be slower but you can use
//         useful development tools. {learnMoreButton}
//       </Text>
//     );
//   } else {
//     return (
//       <Text style={styles.developmentModeText}>
//         You are not in development mode: your app will run at full speed.
//       </Text>
//     );
//   }
// }

// function handleLearnMorePress() {
//   WebBrowser.openBrowserAsync(
//     'https://docs.expo.io/versions/latest/workflow/development-mode/'
//   );
// }

// function handleHelpPress() {
//   WebBrowser.openBrowserAsync(
//     'https://docs.expo.io/versions/latest/workflow/up-and-running/#cant-see-your-changes'
//   );
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  headbar: {
    // height: 100,
    // backgroundColor: "",
    borderColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'space-between'

  },
  itemHeadLeft: {
    backgroundColor: '#ABEBC6',
    padding: 20,
    alignItems: 'center',
    // justifyContent:'flex-start'
  },
  itemHeadRight: {
    backgroundColor: '#D6EAF8',
    padding: 20,
    alignItems: 'center',
    //justifyContent:'flex-start'
  }
});
