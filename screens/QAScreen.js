import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text, View, TextInput, Button
} from 'react-native';
import {MonoText} from "../components/StyledText";
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { SearchBar, Header } from 'react-native-elements';

const items = [
    // this is the parent or 'item'
    {
        name: 'Fruits',
        id: 0,
        // these are the children or 'sub items'
        children: [
            {
                name: 'Apple',
                id: 10,
            },
            {
                name: 'Strawberry',
                id: 17,
            },
            {
                name: 'Pineapple',
                id: 13,
            },
            {
                name: 'Banana',
                id: 14,
            },
            {
                name: 'Watermelon',
                id: 15,
            },
            {
                name: 'Kiwi fruit',
                id: 16,
            },
        ],
    }
];

export default class QAScreen extends Component {
    constructor() {
        super();
        this.state = {
            selectedItems: [],
        };
    }
    onSelectedItemsChange = (selectedItems) => {
        this.setState({ selectedItems });
    };
    render() {
        return (
            <View style={styles.container}>
                <Header
                    leftComponent={{
                        icon: 'g-translate',
                        color: '#000',
                        onPress: () => this.props.navigation.navigate('Cart'),
                    }}
                    centerComponent={{ text: 'Pani Candle', style: { color: '#000', fontSize: 20 } }}
                    rightComponent={{
                        icon: 'shopping-basket',
                        color: '#000',
                        onPress: () => this.props.navigation.navigate('Cart'),
                    }}
                    backgroundColor="#A7FFFC"

                />
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <View style={styles.qa} >
                        <Text>Question</Text>
                        <SectionedMultiSelect
                            items={items}
                            uniqueKey="id"
                            subKey="children"
                            selectText="Question1"
                            showDropDowns={true}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={this.onSelectedItemsChange}
                            selectedItems={this.state.selectedItems}
                            single={true}
                        />
                        <SectionedMultiSelect
                            items={items}
                            uniqueKey="id"
                            subKey="children"
                            selectText="Question2"
                            showDropDowns={true}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={this.onSelectedItemsChange}
                            selectedItems={this.state.selectedItems}
                            single={true}
                        />
                        <SectionedMultiSelect
                            items={items}
                            uniqueKey="id"
                            subKey="children"
                            selectText="Question3"
                            showDropDowns={true}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={this.onSelectedItemsChange}
                            selectedItems={this.state.selectedItems}
                            single={true}
                        />
                    </View>
                    <View style={styles.search}>
                        <Text>Answer</Text>
                        <View style={styles.textAreaContainer} >
                            <TextInput
                                style={styles.textArea}
                                underlineColorAndroid="transparent"
                                placeholder="answer when user select question"
                                placeholderTextColor="grey"
                                numberOfLines={10}
                                multiline={true}
                                editable = {false}
                            />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

QAScreen.navigationOptions = {
    header: null
    // headerLeft: (
    //     <Button
    //         onPress={() => alert('Change Language')}
    //         title="TH/EN"
    //         color="#1456EB"/>
    // ),
    // headerRight: (
    //     <Button
    //         onPress={() => alert('This is a button!')}
    //         title="CART"
    //         color="#1456EB"
    //     />),
    // title: "Pani Candle",
    // headerTitleStyle: {
    //     textAlign:"center",
    //     flex:1
    // },
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    search: {
        margin: 10,
        height: 1,

        flex: 1
    },
    qa: {
        margin: 10,
        height: 1,
        flex: 2,
        borderColor: '#000000',
        backgroundColor: '#ffffff'
    },
    textAreaContainer: {
        borderColor: '#000000',
        borderWidth: 1,
        padding: 5
    },
    textArea: {
        height: 100,
        justifyContent: "flex-start"
    }

});