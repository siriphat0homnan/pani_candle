import React, {Component} from 'react';
import {ScrollableTabView} from '@valdio/react-native-scrollable-tabview';
import {
    Button,
    ScrollView, StyleSheet,
    Text,
    View,
} from 'react-native';
import { SearchBar, Header } from 'react-native-elements';
import ContactScreen from "./ContactScreen";

export default class SettingsScreen extends Component {

    //execute callback in order to stop the refresh animation.
    _onRefresh = (callback) => {
        networkRequest().then(response => callback(response))
    }

    render() {
        return(
            <View style={styles.container}>
                <Header
                    leftComponent={{
                        icon: 'g-translate',
                        color: '#000',
                        onPress: () => this.props.navigation.navigate('Cart'),
                    }}
                    centerComponent={{ text: 'Pani Candle', style: { color: '#000', fontSize: 20 } }}
                    rightComponent={{
                        icon: 'shopping-basket',
                        color: '#000',
                        onPress: () => this.props.navigation.navigate('Cart'),
                    }}
                    backgroundColor="#A7FFFC"

                />
        <ScrollableTabView
            refreshControlStyle={{backgroundColor: 'red'}}
            pullToRefresh={this._onRefresh}
        >
            <ScrollView tabLabel="Personal Information" >
                <View>
                    <Text>Detail about user</Text>
                </View>
            </ScrollView>
            <ScrollView tabLabel="Order status tracking" >
                <View>
                    <Text>Detail about status tracking of order</Text>
                </View>
            </ScrollView>

        </ScrollableTabView>
        </View>)
    }
}
SettingsScreen.navigationOptions = {
    header: null
    // headerLeft: (
    //     <Button
    //         onPress={() => alert('Change Language')}
    //         title="TH/EN"
    //         color="#1456EB"/>
    // ),
    // headerRight: (
    //     <Button
    //         onPress={() => alert('This is a button!')}
    //         title="CART"
    //         color="#1456EB"
    //     />),
    // title: "Pani Candle",
    // headerTitleStyle: {
    //     textAlign:"center",
    //     flex:1
    // },
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});